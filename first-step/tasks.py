from celery import Celery

# create a celery app
app = Celery(
    'tasks', # module name
    backend='rpc://', # result backend
    broker='amqp://localhost' # broker url
)

"""
# config app (optional)
## single config
app.conf.task_serializer = 'json'
## multiple config
app.conf.update(
    task_serializer='json',
    accept_content=['json'], # Ignore other content
    result_serializer='json',
    timezone='Europe/Oslo',
    enable_utc=True,
)
## config file
app.config_from_object('celeryconfig')
"""

### define task for app
@app.task
def add(x, y):
    return x + y
