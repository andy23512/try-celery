from tasks import add
result = add.delay(4, 4) # call a task of app
print(result.ready()) # get process status
print(result.get(timeout=10)) # wait for the result to complete
