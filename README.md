# Celery note and sample program

## Tutorial
[First Steps with Celery](http://docs.celeryproject.org/en/latest/getting-started/first-steps-with-celery.html#first-steps)

## Setup
### Install esl-erlang
[Install Erlang](https://packages.erlang-solutions.com/erlang/#tabs-debian)
```
wget https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb
sudo dpkg -i erlang-solutions_1.0_all.deb
sudo apt-get update
sudo apt-get install esl-erlang
```
### Install RabbitMQ
[Install RabbitMQ on Debian / Ubuntu](https://www.rabbitmq.com/install-debian.html)
```
echo "deb https://dl.bintray.com/rabbitmq/debian xenial main" | sudo tee /etc/apt/sources.list.d/bintray.rabbitmq.list
wget -O- https://dl.bintray.com/rabbitmq/Keys/rabbitmq-release-signing-key.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get install rabbitmq-server
```

### Start RabbitMQ Server
```
sudo service rabbitmq-server start
```

## Install Celery
```
pip install celery
```

## Commands

### Worker

#### Run a Celery worker (require rerun after celery app modified)
```
celery -A [project-name/app-name] worker -l info -Q [queue-name(s)]
```

#### Control multiple Celery workers
```
celery multi start w1 -A [project-name] -l info
celery multi restart w1 -A [project-name] -l info
celery multi stop w1 -A [project-name] -l info
celery multi stopwait w1 -A [project-name] -l info
```

#### Split log file for workers
```
mkdir -p /var/run/celery
mkdir -p /var/log/celery
celery multi start w1 -A [project-name] -l info --pidfile=/var/run/celery/%n.pid \
                                                --logfile=/var/log/celery/%n%I.log
```

#### Specify arguments for different workers
```
celery multi start 10 -A [project-name] -l info -Q:1-3 images,video -Q:4,5 data \
    -Q default -L:4,5 debug
```

#### Set rate limit
```
celery -A [app-name] control rate_limit [app-name].[task-name] 10/m
```

#### See what tasks the worker(s) is(are) currently working on
```
celery -A proj inspect active # all workers
celery -A proj inspect active --destination=celery@example.com # specify worker
```

#### monitoring tasks and workers
```
celery -A proj control enable_events # force workers to enable event messages
celery -A proj events --dump # start the event dumper to see what the workers are doing
celery -A proj events # start the curses interface
celery -A proj control disable_events # disable events
```

#### list workers
```
celery -A proj status
```

### Other command

#### Check syntax of celeryconfig
```
python -m celeryconfig
```

