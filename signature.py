from proj.tasks import add

s1 = add.s(2, 2) # complete parameter call (s: signature)
res = s1.delay()
res.get()

s2 = add.s(2) # partial parameter call (add(?, 2))
res = s2.delay(8) # call with the other parameter (add(8, 2))
res.get()

"""
s3 = add.s(2, 2, debug=True)
s3.delay(debug=False) # overriding keyword argument
"""
