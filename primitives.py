from celery import group, chain, chord
from proj.tasks import add, mul, xsum

# group: A group calls a list of tasks in parallel, and it returns a special
# result instance that lets you inspect the results as a group, and retrieve
# the return values in order.
print(group(add.s(i, i) for i in range(10))().get())
g = group(add.s(i) for i in range(10)) # partial group
print(g(10).get())

# chain: Tasks can be linked together so that after one task returns the other
# is called
print(chain(add.s(4, 4) | mul.s(8))().get()) # (4 + 4) * 8
print((add.s(4, 4) | mul.s(8))().get()) # same as previous line
g = chain(add.s(4) | mul.s(8)) # (? + 4) * 8
print(g(4).get())

"""
# chord: A chord is a group with a callback (Result backends that supports
# chords: Redis, Database, Memcached, and more.)
print(chord((add.s(i, i) for i in range(10)), xsum.s())().get())
(group(add.s(i, i) for i in range(10)) | xsum.s())().get() # would be converted to chord
"""
