broker_url = 'amqp://' # broker url
result_backend = 'rpc://' # result backend
include = ['proj.tasks'] # modules to import when worker starts
result_expires = 3600

task_serializer = 'json'
result_serializer = 'json'
accept_content = ['json']
timezone = 'Asia/Taipei'
enable_utc = True

"""
task_routes = {
    'tasks.add': 'low-priority', # assign all add task to low-priority queue
}
"""

"""
def my_on_failure(self, exc, task_id, args, kwargs, einfo):
    print('Oh no! Task failed: {0!r}'.format(exc))
task_annotations = {
    'tasks.add': {'rate_limit': '10/m'},
    '*': {
        'rate_limit': '10/m',
        'on_failure': my_on_failure # failure callback
    }
}
"""

### Object format option
"""
class MyAnnotate(object):
    def annotate(self, task):
        if task.name.startswith('tasks.'):
            return {'rate_limit': '10/s'}

task_annotations = (MyAnnotate(),)
"""
