from proj.tasks import add
from proj.celery import app

### call
res = add.delay(2, 2) # normal call
print(res.get(timeout=1)) # get result (if result backend is configured)
print(res.id) # get task id
add.apply_async((3, 3)) # same as previous one, with more options
add.apply_async(
    (4, 4),
    # queue='lopri', # specify which queue to send
    countdown=10 # execute at least 10 second after receiving message
)
add(5, 5) # execute in current process
res3 = add.delay(2, 'aa')
print(res3.get(propagate=False)) # stop error propagation
print(res3.failed())
print(res3.successful())
print(res3.state)
"""
res2 = add.delay(2)
print(res2.get(timeout=1)) # get expection
"""

### get result from app
res = app.AsyncResult('this-id-does-not-exist')
print(res.state)
